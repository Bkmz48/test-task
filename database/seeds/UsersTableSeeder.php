<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $now = Carbon::now();

    $users = [
      [
        'id' => 1,
        'name' => 'Admin',
        'email' => 'test@exemple.com',
        'password' => bcrypt('12345678'),
        'created_at' => $now,
        'updated_at' => $now,
      ],
      [
        'id' => 2,
        'name' => 'User1',
        'email' => 'test2@exemple.com',
        'password' => bcrypt('12345678'),
        'created_at' => $now,
        'updated_at' => $now,
      ],
    ];
    DB::table('users')->insert($users);
  }
}
