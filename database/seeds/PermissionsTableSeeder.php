<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $now = Carbon::now();

    $permissions = [
      [
        'id' => 1,
        'name' => 'test',
        'guard_name' => 'web',
        'created_at' => $now,
        'updated_at' => $now,
      ],
    ];
    DB::table('permissions')->insert($permissions);
  }
}
