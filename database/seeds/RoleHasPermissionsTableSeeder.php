<?php

use Illuminate\Database\Seeder;

class RoleHasPermissionsTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $role_has_permissions = [
      ['permission_id' => 1, 'role_id' => 1],
    ];
    DB::table('role_has_permissions')->insert($role_has_permissions);
  }
}
