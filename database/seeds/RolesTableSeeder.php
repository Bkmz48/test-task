<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $now = Carbon::now();

    $roles = [
      [
        'id' => 1,
        'name' => 'admin',
        'guard_name' => 'web',
        'created_at' => $now,
        'updated_at' => $now,
      ],
    ];
    DB::table('roles')->insert($roles);
  }
}
