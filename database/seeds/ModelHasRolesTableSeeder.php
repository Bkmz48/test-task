<?php

use Illuminate\Database\Seeder;

class ModelHasRolesTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $model_has_roles = [
      ['role_id' => 1, 'model_id' => 1, 'model_type' => 'App\\User'],
    ];

    DB::table('model_has_roles')->insert($model_has_roles);
  }
}
