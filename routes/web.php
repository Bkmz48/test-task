<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Company
Route::post('company/store', 'CompanyController@store')
  ->name('company.store');
Route::post('company/{company}/update', 'CompanyController@update')
  ->name('company.update');
Route::post('company/{company}/delete', 'CompanyController@delete')
  ->name('company.delete');

Route::post('company/{company}/edit', 'CompanyController@edit')
  ->name('company.edit');

Route::post('companies/getData', 'CompanyController@getData')
  ->name('company.getData');
Route::post('company/employers', 'CompanyController@getEmployers')
  ->name('company.getEmployers');
Route::get('company/{company}', 'CompanyController@show')
  ->name('company.show');
Route::get('companies', 'CompanyController@list')
  ->name('company.list');

//User
Route::get('user/{user}', 'UserController@show')
  ->name('user.show');
Route::get('user/{user}/edit', 'UserController@edit')
  ->name('user.edit');
Route::post('user/{user}/update', 'UserController@update')
  ->name('user.update');
Route::get('user/{user}/delete', 'UserController@delete')
  ->name('user.delete');