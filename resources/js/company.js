import Company from './components/company/Company';

if (document.getElementById('company-list')) {
  new Vue({
    el: '#company-list',
    components: {
      Company
    },
  });
}

