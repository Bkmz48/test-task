<?php

return [
  'name' => 'Name',
  'email' => 'Email',
  'employee' => 'Employee of company',
  'edit' => 'Edit',
  'delete' => 'Delete',
  'chose_company' => 'Chose company',
];
