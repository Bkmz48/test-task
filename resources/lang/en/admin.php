<?php

return [
  'edit' => 'Edit',
  'create' => 'Create',
  'save' => 'Save',
  'delete' => 'Delete',
  'name' => 'Name',
  'company' => 'Company',
  'employees' => 'Employees',
  'chose_employees' => 'Chose employees',
  'confirm_delete' => 'Confirm delete',
  'hide' => 'Hide',
];
