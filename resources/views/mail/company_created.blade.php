@component('mail::message')
@lang('mail.company_created') {{ $company->name}}
@component('mail::button', ['url' => route('company.show',$company)])
@lang('mail.company_show')
@endcomponent
@endcomponent
