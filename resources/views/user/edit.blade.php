@extends('layouts.master')

@section('content')
    <div class="container mt-4">
        <h1 class="text-center">@lang('user.edit')</h1>
        <hr>
        <form method="post"
              action="{{ route('user.update', $user) }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="font-weight-bold">@lang('user.name')<span class="text-danger h4">*</span></label>
                <input type="text"
                       value="{{$user->name}}"
                       class="form-control"
                       placeholder="@lang('admin.name')"
                       name="name">
            </div>
            <div class="form-group">
                <label class="font-weight-bold">@lang('user.email')<span class="text-danger h4">*</span></label>
                <input type="text"
                       value="{{$user->email}}"
                       class="form-control"
                       placeholder="@lang('user.email')"
                       name="email">
            </div>
            <div class="form-group">
                <label class="font-weight-bold">@lang('user.employee')</label>
                <select  name="company_id" class="form-control">
                    <option value="">@lang('user.chose_company')</option>
                    @foreach($companies as $company)
                        <option value="{{$company->id}}" {{($user->company_id == $company->id ? 'selected' : '')}}>{{$company->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="@lang('admin.save')">
            </div>
        </form>
    </div>
@endsection
