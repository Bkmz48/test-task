@extends('layouts.master')

@section('content')
    <div class="container mt-4">
        <p>@lang('user.name'): {{$user->name}}</p>
        <p>@lang('user.email'): {{$user->email}}</p>
        @if($user->employer)
            <p>@lang('user.employee'): {{$user->employer->name}}</p>
        @endIf
        <a href="{{route('user.edit',$user)}}" class="btn btn-success brn-sm"> @lang('user.edit')</a>
        <a href="{{route('user.delete',$user)}}" class="btn btn-danger brn-sm"> @lang('user.delete')</a>
    </div>
@endsection
