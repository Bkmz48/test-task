<?php

use Illuminate\Support\Facades\DB;

function string_generate($length = 10, $type = null) {
  $numbers = '0123456789';
  $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  if ($type == 'numbers') {
    $chars = $numbers;
  }
  elseif ($type == 'letters') {
    $chars = $letters;
  }
  else {
    $chars = $numbers . $letters;
  }

  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}

function api_key_generate() {
  while (true) {
    $api_key = string_generate(15);

    //check that key is unique
    $check = DB::table('companies')
      ->where('api_key', '=', $api_key)
      ->get();

    //out of while if key is unique
    if (count($check) == 0) {
      break;
    }
  }

  return $api_key;
}