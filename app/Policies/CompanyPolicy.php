<?php

namespace App\Policies;

use App\Company;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
  use HandlesAuthorization;

  /**
   * Create a new policy instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  public function checkCompanyPermission(User $curr, Company $company)
  {
    if($curr == null) {
      return false;
    }

    if($curr->hasRole('admin')) {
      return true;
    }

    if($curr->id == $company->user_id) {
      return true;
    }

    return false;
  }

}
