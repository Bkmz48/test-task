<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserProfile
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
      //
    }

    public function checkUserPermission(User $curr, User $profile)
    {
        if($curr == null) {
            return false;
        }

        if($curr->hasRole('admin')) {
            return true;
        }

        if($curr->id == $profile->id) {
            return true;
        }

        return false;
    }

}
