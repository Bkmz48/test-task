<?php

namespace App;

use App\Events\DeleteUserEvent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable {

  use Notifiable, HasRoles;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'email',
    'company_id',
    'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  protected $dispatchesEvents = [
    'deleting' => DeleteUserEvent::class,
  ];

  public function companies() {
    return $this->hasMany(Company::class);
  }

  public function employer() {
    return $this->belongsTo(Company::class, 'company_id');
  }

  static function getAdmin() {
    return User::role('admin')->first();
  }

}
