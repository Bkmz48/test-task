<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use JavaScript;

class CompanyController extends Controller {

  public function __construct() {
    $this->middleware('can:checkCompanyPermission,company')
      ->except('getEmployers', 'list', 'edit', 'getData', 'store');

    $this->middleware('auth')->only('list', 'edit', 'getData', 'store');
  }

  public function edit(Request $request, Company $company) {
    return response()->json([
      'company' => $company,
      'employees_ids' => $company->getEmployeesIds(),
      'users' => User::all(),
    ]);
  }

  public function store(Request $request) {
    $data = $this->validate($request, [
      'name' => 'required|string|max:255',
      'employees' => 'nullable|array',
    ]);

    $company = new Company;
    $company->name = $data['name'];
    $company->user_id = $request->user()->id;
    $company->api_key = api_key_generate();
    $company->save();

    $company->processEmployees($data['employees'] ?? []);

    return back();
  }

  public function update(Request $request, Company $company) {
    $data = $this->validate($request, [
      'name' => 'required|string|max:255',
      'employees' => 'nullable|array',
    ]);

    $company->name = $data['name'];
    $company->save();

    $company->processEmployees($data['employees'] ?? []);

    return back();
  }

  public function delete(Company $company) {
    $company->delete();

    return response()->json('success!');
  }

  public function show(Company $company) {
    $data['company'] = $company->load('employees');

    return view('company.show', $data);
  }

  public function list(Request $request) {
    JavaScript::put([
      'users' => User::all(),
    ]);

    return view('company.list');
  }

  public function getEmployers(Request $request) {
    $data = $this->validate($request, [
      'api_key' => 'required|string|max:255|exists:companies',
    ]);

    $employees = User::whereHas('employer', function ($query) use ($data) {
      $query->where('api_key', $data['api_key']);
    })
      ->paginate(15);

    return response()->json($employees);
  }

  public function getData(Request $request) {
    $companies = $request->user()->companies;

    return response()->json($companies);

  }
}
