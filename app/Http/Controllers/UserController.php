<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller {

  public function __construct() {
    $this->middleware('can:checkUserPermission,user');
  }

  public function show(User $user) {
    $data['user'] = $user->load('employer');
    return view('user.show', $data);
  }

  public function edit(User $user) {
    $data['user'] = $user->load('company');
    $data['companies'] = Company::all();

    return view('user.edit', $data);
  }

  public function update(Request $request, User $user) {
    $data = $this->validate($request, [
      'name' => 'required|string|max:255',
      'email' => [
        'required',
        'string',
        'max:255',
        Rule::unique('users')->ignore($user->id),
      ],
      'company_id' => 'nullable|string|max:255',
    ]);

    $user->fill($data);
    $user->save();

    return back();
  }

  public function delete(User $user) {
    $user->delete();

    return redirect(route('home'));
  }
}
