<?php

namespace App\Listeners;

use App\Events\CreateCompanyEvent;
use App\Notifications\CompanyCreated;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateCompany {

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Handle the event.
   *
   * @param  CreateCompanyEvent $event
   *
   * @return void
   */
  public function handle(CreateCompanyEvent $event) {
    \Notification::route('mail', User::getAdmin()->email ?? '')
      ->notify(new CompanyCreated($event->company));
  }
}
