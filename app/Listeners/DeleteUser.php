<?php

namespace App\Listeners;

use App\Events\DeleteUserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteUser {

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Handle the event.
   *
   * @param  DeleteUserEvent $event
   *
   * @return void
   */
  public function handle(DeleteUserEvent $event) {
    $companies = $event->user->companies;
    foreach ($companies as $company) {
      $company->delete();
    }
  }
}
