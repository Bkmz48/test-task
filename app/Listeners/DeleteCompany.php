<?php

namespace App\Listeners;

use App\Events\DeleteCompanyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteCompany {

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Handle the event.
   *
   * @param  DeleteCompanyEvent $event
   *
   * @return void
   */
  public function handle(DeleteCompanyEvent $event) {
    $event->company->processEmployees();
  }
}
