<?php

namespace App;

use App\Events\CreateCompanyEvent;
use App\Events\DeleteCompanyEvent;
use Illuminate\Database\Eloquent\Model;

class Company extends Model {

  protected $dispatchesEvents = [
    'deleting' => DeleteCompanyEvent::class,
    'create' => CreateCompanyEvent::class,
  ];

  public function owner() {
    return $this->belongsTo(User::class);
  }

  public function employees() {
    return $this->hasMany(User::class);
  }

  /**
   * Attach or detach user as employee
   *
   * @param array $employees_ids
   */
  public function processEmployees($employees_ids = []) {
    $employees = $this->employees;
    foreach ($employees as $employee) {
      $employee->company_id = null;
      $employee->save();
    }

    if (!empty($employees_ids)) {
      $employees = User::whereIn('id', $employees_ids)->get();
      foreach ($employees as $employee) {
        $employee->company_id = $this->id;
        $employee->save();
      }
    }
  }

  public function getEmployeesIds() {
    return $this->employees->pluck('id');
  }
}
